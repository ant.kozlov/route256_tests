import sys

_MAX_COORD = 15


class Link:
    def __init__(self, start, end):
        self.start = start
        self.end = end


class Coords:
    def __init__(self, row, col):
        self.row = row
        self.col = col

    def get_id(self):
        return _MAX_COORD * self.row + self.col

    @staticmethod
    def distance(coord1, coord2):
        dr = coord2.row - coord1.row
        dc = coord2.col - coord1.col
        return dr * dr + dc * dc


class Node:
    def __init__(self, coords):
        self._links = []
        self._coords = coords
        self._id = coords.get_id()
        self._gid = 0

    @property
    def coords(self):
        return self._coords

    @property
    def id(self):
        return self._id

    @property
    def gid(self):
        return self._gid

    @property
    def links(self):
        return self._links

    def add_link(self, neighbor_link):
        self._links.append(neighbor_link)

    def traverse(self, gid: int, vertex_list: []):
        self._gid = gid
        vertex_list.append(self._id)

        for link in self._links:
            if link.end.gid == 0:
                link.end.traverse(gid, vertex_list)

    def __repr__(self):
        res = bytearray(f'{self._gid}={self.id}', encoding='UTF-8')
        if len(self.links):
            res += b'->'
        for link in self.links:
            res += bytearray(f'({link.start.id}-{link.end.id})', encoding='UTF-8')
        return str(res, encoding='UTF-8')


def _gen_neighbors_ids(cur_coords, max_row, max_col) -> []:
    res = []
    row = cur_coords.row
    col = cur_coords.col
    # add neighbors ids with bounds checking
    if row != 0 and col != 0:
        res.append(Coords(row - 1, col - 1).get_id())
    if row != 0:
        res.append(Coords(row - 1, col).get_id())
    if row != 0 and col < (max_col - 1):
        res.append(Coords(row - 1, col + 1).get_id())
    if col != 0:
        res.append(Coords(row, col - 1).get_id())
    if col < (max_col - 1):
        res.append(Coords(row, col + 1).get_id())
    if col != 0 and row < (max_row - 1):
        res.append(Coords(row + 1, col - 1).get_id())
    if row < (max_row - 1):
        res.append(Coords(row + 1, col).get_id())
    if row < (max_row - 1) and col < (max_col - 1):
        res.append(Coords(row + 1, col + 1).get_id())

    return res


def _create_graphs_from_matrix(matrix: [], row_num: int, col_num: int):
    nodes = {}
    for i in range(row_num):
        for j in range(col_num):
            v = matrix[i][j]
            if v == 0:
                continue
            else:
                node = Node(Coords(i, j))
                nodes[node.id] = node
    for k in nodes.keys():
        node = nodes[k]
        nc = node.coords
        ids = _gen_neighbors_ids(cur_coords=nc, max_row=row_num, max_col=col_num)

        for cur_id in ids:
            nn = nodes.get(cur_id)
            if nn is None:
                continue
            node.add_link(Link(node, nn))

    return nodes


def _check_3x(nodes, vlist) -> bool:
    if len(vlist) != 3:
        return False
    for v in vlist:
        if len(nodes[v].links) != 2:
            return False
    return True


def _check_5x(nodes, vlist) -> bool:
    if len(vlist) != 5:
        return False
    l1, l2, l3 = 0, 0, 0
    for v in vlist:
        links = nodes[v].links
        vlen = len(links)
        if vlen == 2:
            l2 += 1
        elif vlen == 3:
            l3 += 1
        elif vlen == 1:
            if Coords.distance(links[0].start.coords, links[0].end.coords) != 1:
                return False
            l1 += 1
        else:
            return False
    return l1 == 2 and l2 == 1 and l3 == 2


def _check_7x(nodes, vlist) -> bool:
    if len(vlist) != 7:
        return False
    l1, l2, l3 = 0, 0, 0
    for v in vlist:
        links = nodes[v].links
        vlen = len(links)
        if vlen == 2:
            for ll in links:
                if Coords.distance(ll.start.coords, ll.end.coords) != 1:
                    return False
            l2 += 1
        elif vlen == 3:
            l3 += 1
        elif vlen == 1:
            if Coords.distance(links[0].start.coords, links[0].end.coords) != 1:
                return False
            l1 += 1
        else:
            return False
    return l1 == 2 and l2 == 3 and l3 == 2


def _convert_line(line: str) -> []:
    nl = []
    for e in line:
        nl.append(1 if e == '*' else 0)
    return nl


def process_data(aref: list) -> None:
    res = []
    matrix = []

    fsize_line = sys.stdin.readline().strip().split()
    max_row = int(fsize_line[0])
    max_col = int(fsize_line[1])

    for ll in range(0, max_row):
        line = sys.stdin.readline().strip()
        nline = _convert_line(line)
        matrix.append(nline)

    graphs = _create_graphs_from_matrix(matrix, max_row, max_col)
    vertexes = list(graphs.keys())
    gid = 1
    subgraphs_vl = []

    while len(vertexes) > 0:
        vl = []
        cur_id = vertexes[0]
        graphs[cur_id].traverse(gid, vl)
        for v in vl:
            vertexes.remove(v)
        gid += 1
        subgraphs_vl.append(vl)

    for svl in subgraphs_vl:
        if len(svl) == 1:
            res.append(1)
        elif _check_3x(graphs, svl):
            res.append(3)
        elif _check_5x(graphs, svl):
            res.append(5)
        elif _check_7x(graphs, svl):
            res.append(7)
        else:
            res.clear()
            break

    aref.append(res)


def _format_list(aref: list):
    res_str = bytearray()
    for e in aref:
        res_str += bytearray(f'{e} ', encoding='UTF-8')
    return str(res_str, encoding='UTF-8').strip()


def format_data(aref: list) -> None:
    for ds in aref:
        if len(ds) > 0:
            print('YES')
            ds.sort()
            print(_format_list(ds))
        else:
            print('NO')


if __name__ == '__main__':
    result = []
    num = int(sys.stdin.readline().strip())
    for i in range(0, num):
        process_data(result)

    format_data(result)
