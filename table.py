import sys


class Row(object):
    def __init__(self):
        self.cells = list()

    def add_cells_from_str(self, cs: str):
        v = cs.split()
        for vv in v:
            self.cells.append(int(vv))

    def __repr__(self):
        res = bytearray()
        for c in self.cells:
            res += bytearray(f'{c} ', encoding='UTF-8')
        return str(res, encoding='UTF-8').strip()


class Matrix(object):
    def __init__(self):
        self._rows = list()

    def __repr__(self):
        res = bytearray()
        for r in self._rows:
            res += bytearray(f'{r.__repr__()}\n', encoding='UTF-8')
        return str(res, encoding='UTF-8')

    def sort_by_col(self, index: int) -> None:
        self._rows.sort(key=lambda v: v.cells[index])

    def add_row(self, row: Row):
        self._rows.append(row)


def process_data(aref: list) -> None:
    res = Matrix()
    mn_line = sys.stdin.readline().strip()
    num_rows = int(mn_line.split()[0])

    for rr in range(0, num_rows):
        row_str = sys.stdin.readline().strip()
        row = Row()
        row.add_cells_from_str(row_str)
        res.add_row(row)

    num_sorts = int(sys.stdin.readline().strip())
    sorts_str = sys.stdin.readline().strip().split()

    for ss in sorts_str:
        col = int(ss) - 1
        res.sort_by_col(col)

    aref.append(res)


def format_data(aref: list) -> None:
    for ii in aref:
        print(ii)
    print()


if __name__ == '__main__':
    result = []

    num = int(sys.stdin.readline().strip())
    for i in range(0, num):
        sys.stdin.readline()
        process_data(result)

    format_data(result)
