import sys
import re

_REGEX_VALIDATOR = r"^[_a-z0-9][_\-a-z0-9]{1,23}$"
_PATTERN = re.compile(_REGEX_VALIDATOR)
_LOGINS = {}

_TEST1_DATA = [('a', False), ('_tester', True), ('tester', True), ('tester1', True), ('~~~!!!', False), ('911', True),
               ('-test-me', False), ('test_me-93', True), ('?', False),
               ('abcdefghijlmnopqkrstuvwxyz', False)]
_TEST2_DATA = [
    ('1', False),
    ('11', True),
    ('111', True),
    ('a', False),
    ('aa', True),
    ('aaa', True),
    ('_', False),
    ('__', True)
]

_TEST3_DATA = [
    ('one', True),
    ('One', False),
    ('onE', False),
    ('ONE', False),
    ('two', True)
]

_TEST_FORMATTER = [[False, True, False], [True], [False, False], [False]]


def validate_login(login: str) -> bool:
    value = login.lower()
    if _PATTERN.match(value) is not None:
        if _LOGINS.get(value) is not None:
            return False
        else:
            _LOGINS[value] = 1
            return True
    else:
        return False


def process_data(aref: list) -> None:
    res = []
    _LOGINS.clear()
    num_attempts = int(sys.stdin.readline().strip())
    for i in range(0, num_attempts):
        v = sys.stdin.readline().strip()
        res.append(validate_login(v))
    aref.append(res)


def format_data(aref: list) -> None:
    for i in aref:
        for j in i:
            print('yes' if j else 'no')
        print()


def _do_test(name: str, data: []) -> None:
    for i, o in data:
        if validate_login(i) != o:
            print(f'{name} failed')
            break
    print(f'{name} passed')


def _do_tests():
    _do_test('test1', _TEST1_DATA)
    _do_test('test2', _TEST2_DATA)
    _do_test('test3', _TEST3_DATA)
    format_data(_TEST_FORMATTER)
    exit(1)


if __name__ == '__main__':
    # _do_tests()
    result = []
    num = int(sys.stdin.readline().strip())
    for i in range(0,num):
        process_data(result)
    format_data(result)
