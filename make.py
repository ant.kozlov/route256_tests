import sys


class Node:
    def __init__(self, name):
        self._name = name
        self._children = []
        self._built = False

    @property
    def name(self):
        return self._name

    @property
    def children(self):
        return self._children

    def add_child(self, child):
        self._children.append(child)

    def traverse(self, dep):
        if self._built:
            return
        for c in self._children:
            c.traverse(dep)
        if not self._built:
            dep.append(self._name)
            self._built = True


def _get_or_create(nodes, name):
    node = nodes.get(name)
    if node is None:
        node = nodes[name] = Node(name)

    return node


def _process_mod(nodes, name) -> []:
    res = []
    nodes[name].traverse(res)
    return res


def process_data(aref: list) -> None:
    res = []
    nodes_store = {}
    num_targets = int(sys.stdin.readline().strip())

    for tt in range(0, num_targets):
        target_str = sys.stdin.readline().strip().split(sep=':')
        name = target_str[0]
        node = _get_or_create(nodes_store, name)

        if len(target_str) == 2:
            depends = target_str[1].strip().split()
            for d in depends:
                child = _get_or_create(nodes_store, d)
                node.add_child(child)

    nq = int(sys.stdin.readline().strip())
    for q in range(0, nq):
        mod = sys.stdin.readline().strip()
        res.append(_process_mod(nodes_store, mod))

    aref.append(res)


def _format_line(aref: list):
    res = bytearray(f'{len(aref)}', encoding='UTF-8')
    for e in aref:
        res += bytearray(f' {e}', encoding='UTF-8')
    return str(res, encoding='UTF-8')


def format_data(aref: list) -> None:
    for ds in aref:
        for dss in ds:
            print(_format_line(dss))
        print()


if __name__ == '__main__':
    result = []

    num = int(sys.stdin.readline().strip())
    for i in range(0, num):
        sys.stdin.readline()
        process_data(result)

    format_data(result)
