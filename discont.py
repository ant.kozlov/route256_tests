import sys


def process_data(aref: list) -> None:
    res = {}
    nq = int(sys.stdin.readline().strip())
    data = sys.stdin.readline().strip().split()

    for d in data:
        dn = int(d)
        if res.get(dn) is None:
            res[dn] = 1
        else:
            res[dn] = res[dn] + 1

    aref.append(res)


def calc_sum(p: int, c: int) -> int:
    cc = 2*(c//3) + c%3
    return p * cc


def format_data(aref: list) -> None:
    for ii in aref:
        sum = 0
        for k, v in ii.items():
            sum = sum + calc_sum(k, v)
        print(sum)


if __name__ == '__main__':
    result = []
    num = int(sys.stdin.readline().strip())
    for i in range(0, num):
        process_data(result)
    format_data(result)
