import sys

if __name__ == '__main__':
    num = int(sys.stdin.readline().strip())
    result = []

    for line in sys.stdin:
        v = line.strip()
        nums = v.split()
        a = int(nums[0])
        b = int(nums[1])
        s = a + b
        result.append(s)
        num = num - 1
        if num == 0:
            break

    for n in result:
        print(n)
