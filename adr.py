import sys
from bisect import bisect_left, insort_left


def index(a, x):
    ii = bisect_left(a, x)
    if ii != len(a) and a[ii] == x:
        return ii
    raise ValueError


def process_data(aref: list) -> None:
    res = {}
    nq = int(sys.stdin.readline().strip())

    for n in range(0, nq):
        ln = sys.stdin.readline().strip().split()
        name = ln[0]
        phone = ln[1]
        if res.get(name) is None:
            res[name] = [phone]
        else:
            res[name].append(phone)

    aref.append(res)


def trim_data(lref: list) -> []:
    res = []
    while len(res) < 5 and len(lref) > 0:
        cur = lref.pop()
        try:
            ind = res.index(cur)
        except ValueError:
            res.append(cur)
    return res


def format_data(aref: list) -> None:
    for ii in aref:
        keys = sorted(ii.keys())
        for k in keys:
            trimmed = trim_data(ii[k])
            res = bytearray(f'{k}: {len(trimmed)}', encoding='UTF-8')
            for vv in trimmed:
                res += (bytearray(f' {vv}', encoding='UTF-8'))
            print(str(res, encoding='UTF-8'))
        print()


if __name__ == '__main__':
    result = []
    num = int(sys.stdin.readline().strip())
    for i in range(0, num):
        process_data(result)
    format_data(result)
