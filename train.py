import sys
from bisect import bisect_left, insort_left

_VAGON = []
_FREE_ROOMS = []


def init_vagon(num_rooms: int) -> None:
    _VAGON.clear()
    _FREE_ROOMS.clear()
    for nr in range(0, num_rooms):
        _VAGON.append([False, False])
        _FREE_ROOMS.append(nr)


def index(a, x):
    return a.index(x)
    ii = bisect_left(a, x)
    if ii != len(a) and a[ii] == x:
        return ii
    raise ValueError


def book_pos(pos: int) -> bool:
    nr = pos // 2
    ns = pos % 2
    if not _VAGON[nr][ns]:
        _VAGON[nr][ns] = True
        try:
            ii = index(_FREE_ROOMS, nr)
            _FREE_ROOMS.pop(ii)
        except ValueError:
            pass
        return True
    else:
        return False


def book_room(nr: int) -> None:
    _VAGON[nr][0] = _VAGON[nr][1] = True


def find_first_free_room() -> int:
    if len(_FREE_ROOMS) == 0:
        return -1
    else:
        return _FREE_ROOMS.pop(0)


def release_pos(pos: int) -> bool:
    nr = pos // 2
    ns = pos % 2
    if _VAGON[nr][ns]:
        _VAGON[nr][ns] = False
        if not _VAGON[nr][0] and not _VAGON[nr][1]:
            insort_left(_FREE_ROOMS, nr)
        return True
    else:
        return False


# 1m30s
# 1m34s
# 1m30s
# 18s617ms
def process_data(aref: list) -> None:
    res = []
    v = sys.stdin.readline().strip().split()
    nr = int(v[0])
    nq = int(v[1])
    init_vagon(nr)
    for n in range(0, nq):
        ln = sys.stdin.readline().strip().split()
        cmd = int(ln[0])
        if cmd == 3:
            ffr = find_first_free_room()
            if ffr >= 0:
                book_room(ffr)
            res.append((not (ffr < 0), ffr))
        else:
            pos = int(ln[1]) - 1
            if cmd == 1:
                res.append((book_pos(pos), -1))
            elif cmd == 2:
                res.append((release_pos(pos), -1))

    aref.append(res)


def format_data(aref: list) -> None:
    for ii in aref:
        for r, rn in ii:
            res = 'SUCCESS' if r else 'FAIL'
            if rn < 0 or not r:
                print(res)
            else:
                rn = rn * 2 + 1
                print(f'{res} {rn}-{rn + 1}')
        print()


if __name__ == '__main__':
    result = []
    num = int(sys.stdin.readline().strip())
    for i in range(0, num):
        sys.stdin.readline()
        process_data(result)
    format_data(result)
